package com.urs.model.course;

import java.util.*;

public class Course { 
	String courseId;
	String courseName;
	String department ;
	String fee;
	int registered;
    int limit ;
    int open ;
    Map <String,String > semester_proffesor;
    
    public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public int getRegistered() {
		return registered;
	}
	public void setRegistered(int registered) {
		this.registered = registered;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getOpen() {
		return open;
	}
	public void setOpen(int open) {
		this.open = open;
	}
	public Map<String, String> getSemester_proffesor() {
		return semester_proffesor;
	}
	public void setSemester_proffesor(Map<String, String> semester_proffesor) {
		this.semester_proffesor = semester_proffesor;
	}
	
}
