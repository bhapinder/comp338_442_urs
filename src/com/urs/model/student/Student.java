package com.urs.model.student;

public class Student {
private String studentId ;
private String lastNAme ;
private String FirstNAme ;
private String department ;

public String getstudentId() {
	return studentId;
}
public void setstudentId(String studentId) {
	this.studentId = studentId;
}
public String getLastNAme() {
	return lastNAme;
}
public void setLastNAme(String lastNAme) {
	this.lastNAme = lastNAme;
}
public String getFirstNAme() {
	return FirstNAme;
}
public void setFirstNAme(String firstNAme) {
	FirstNAme = firstNAme;
}
public String getDepartment() {
	return department;
}
public void setDepartment(String department) {
	this.department = department;
}

}
