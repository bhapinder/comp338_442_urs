package com.urs.model.registeration;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import java.sql.Connection;
import com.urs.model.course.Course;
import com.urs.model.student.Student;

public class RegisterationDAO { 

	public Connection getConnection(){		
		String driverName ="com.mysql.jdbc.Driver";
		String conURL = "jdbc:mysql://localhost:3306/student";
		String user = "root";
		String pass = "password";
		try {
			Class.forName(driverName).newInstance();
			conn = DriverManager.getConnection(conURL, user, pass);
		} catch (Exception e) {
			e.printStackTrace();
		} 		   

		return conn;

	}

	Connection conn;

	String getPassword(String username) { 
		String password = new String();
		try{
			//		Statement stmt = conn.createStatement();
			Statement stmt = getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT password FROM test.user_credentials where username='"+username+"'");
			rs.next();
			password = rs.getString("password"); 
			conn.close();
			System.out.println("Password for user is "+password);
		}catch(Exception e){
			System.out.println("database error "+e);
		}
		return password;
	}
	ArrayList<String> getDepList(){  
		List<String> deplist = new ArrayList<String>(); 
		try{
			Statement stmt = getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT distinct department FROM test.course;");
			while(rs.next()){
				deplist.add(rs.getString("course.department"));
			}
			System.out.println(" deptlist is  "+deplist);
			conn.close();
		}catch(Exception e){
			System.out.println("database error"+e);
		}


		return new ArrayList<String>();
	}
	ArrayList<Course> getCoursesByDept(String deptName){
		List<String> coursesbyDept = new ArrayList<String>();
		try{
			Statement stmt = getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("select course.coursename ,proffessor.professorname ,proffessor.semester ,course.fee,course.registered,course.limit ,course.open from test.course JOIN test.proffessor ON course.courseId = proffessor.courseid where course.department='"+deptName+"';");
			while(rs.next()){
				coursesbyDept.add(rs.getString("course.coursename")) ;
				coursesbyDept.add(rs.getString("proffessor.professorname")) ;
				coursesbyDept.add(rs.getString("proffessor.semester")) ;
				coursesbyDept.add(rs.getString("course.fee")) ;
				coursesbyDept.add(rs.getString("course.registered")) ;
				coursesbyDept.add(rs.getString("course.limit")) ;
				coursesbyDept.add(rs.getString("course.open")) ;				
			}
			System.out.println("course by department='"+deptName+"' are : "+coursesbyDept);
			conn.close();
		}catch(Exception e){
			System.out.println("database error on "+e);
		}
		return new ArrayList<Course>();
	}
	boolean  enrollCourse(String courseName , String studentId){
		
		return true;
	};
	ArrayList<Course> getCourseListByStudentId(String studentId){
		List<String> courseListbyStuId = new ArrayList<String>();
		try{ 
			Statement stmt = getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("select student.LastName ,student.FirstName ,student.Department ,course.courseName FROM test.student JOIN test.course ON student.courseId = course.courseId where student.studentId =' "+studentId+"';"); 
			while(rs.next()){
				courseListbyStuId.add(rs.getString("LastName"));
				courseListbyStuId.add(rs.getString("FirstName"));
				courseListbyStuId.add(rs.getString("Department"));
				courseListbyStuId.add(rs.getString("courseName"));			
				
			}
			System.out.println("student info "+courseListbyStuId);
		}catch (Exception e){
			System.out.println("database error "+e);
		}
		
		return new ArrayList<Course>();
	}
	boolean feePaymentDetails(String studentId,String courseId ,String carnumber , Date expdate ,String cvv){
		return true;
	}
}
