package com.urs.model.registeration;

import java.util.*;

import com.urs.model.course.Course;

public class RegisterationController {
	boolean authUser(String username ,String password){
		RegisterationDAO regdao = new RegisterationDAO();
		String dbPassword = regdao.getPassword(username);
		if(dbPassword == null ){
			System.out.println("user doesnot exist");
		}else if (dbPassword.equals(password)){
			System.out.println("user is authenticated");
		}
		return true ;
	}
	List<String> getDepartmentDetails(){
		RegisterationDAO rgdao=new RegisterationDAO();
		ArrayList<String> ar=new ArrayList<String>();
		ar=rgdao.getDepList();
		return new ArrayList<String>(); //return deptlst to jsp page
	} 
	
	List<Course> getCOurseDetails(String dept){
		RegisterationDAO rgdao1=new RegisterationDAO();
		ArrayList<Course> ar1=new ArrayList<Course>();
		ar1 =rgdao1.getCoursesByDept(dept);		
		return new ArrayList<Course>(); // return course details to jsp page
	}
	boolean checkCourseEligibility(String studentId ,String courseId){
		return true ;
	}
	boolean enrollCourse(String studentId ,String courseId ,String carnumber , Date expdate ,String cvv){
		return true ;
	}
}
