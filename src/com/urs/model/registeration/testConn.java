package com.urs.model.registeration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;  
import java.sql.SQLException;
import java.sql.Statement;

import com.urs.model.student.Student;

class DataAccess {

private Connection conn; 
public DataAccess(){
    connect();
}


//private User user = new User();


public boolean connect() {
    boolean success = true;

    String driverName ="com.mysql.jdbc.Driver";
    String conURL = "jdbc:mysql://localhost:3306/student";
    String user = "root";
    String pass = "password";

    try {
        Class.forName(driverName).newInstance();
    } catch (InstantiationException e) {
        e.printStackTrace();
    } catch (IllegalAccessException e) {
        e.printStackTrace();
    } catch (ClassNotFoundException e) {
        System.err.println(e.getMessage() + "------Cannot Load Driver");
        success = false;
    }

    try {
        conn = DriverManager.getConnection(conURL, user, pass);
    } catch (SQLException e) {
        System.err.println(e.getMessage() + "--SQL States: " + e.getSQLState() + "---- ErrorCode: " + e.getErrorCode());
        success = false;
    }
    return success;
}


public void selectProfessor () {
    try {
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery("SELECT * FROM test.student");
        while (rs.next()) {
            

            int getStudentId = rs.getInt(1);
            String getLname = rs.getString(2);
           
            System.out.println("....."+getStudentId);
            System.out.println("....."+getLname);
        } 
      
    } catch (SQLException e) {
        e.printStackTrace();
    }
   
	
} 
 

public void selectStudents(){
	try {
    Statement stm = conn.createStatement();
    ResultSet rs = stm.executeQuery("SELECT * FROM test.students");
    while (rs.next()) {
        Student stud = new Student();
        stud.setstudentId(rs.getString("studentId"));
        stud.setLastNAme(rs.getString("LastName")); 
        stud.setFirstNAme("FirstName");
        stud.setDepartment("Department");
        
             
    }
} catch (SQLException e) {
    e.printStackTrace();
}
}

 }

public class testConn  {
	 public static  void main(String args[]){
		 RegisterationDAO rdao = new RegisterationDAO();
		 rdao.getPassword("bsingh2"); 
		 rdao.getDepList();
		 rdao.getCoursesByDept("CS");
	 }
	
	
}
